package Control;

import Model.Monom;
import Model.Polinom;

import java.security.SecureRandom;

import static org.junit.jupiter.api.Assertions.*;

class ControlTest {
    private static Control c;
    private static int nrTesteExecutate = 0;
    private static int nrTesteCuSucces = 0;


    @org.junit.jupiter.api.BeforeAll
    public static void setUpBeforeClass() throws Exception {
        c=new Control();
    }
    @org.junit.jupiter.api.AfterAll
    public static void tearDownAfterClass() throws Exception {
        System.out.println("S-au executat " + nrTesteExecutate + " teste din care "+ nrTesteCuSucces + " au avut succes!");
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {

        nrTesteExecutate++;
    }

    @org.junit.jupiter.api.Test
    void minus() {
       c.citire("3*X^4 +1*X^2 -1*X^1 5*X^0",1);
       c.citire("3*X^2 +1*X^0",2);
       Polinom p1=new Polinom();
       Polinom p2=new Polinom();
       p1=c.getP1();
       p2=c.getP2();
       Polinom p3=new Polinom();
       p3=c.minus(p1,p2);

        String s=c.afisare(p3);
        assertNotNull(s);
        assertEquals(s,"+3.0X^4 -2.0X^2 -1.0X^1 +4.0X^0 ");


        nrTesteCuSucces++;

    }

    @org.junit.jupiter.api.Test
    void plus() {
        c.citire("3*X^4 +1*X^2 -1*X^1 5*X^0",1);
        c.citire("3*X^2 +1*X^0",2);
        Polinom p1=new Polinom();
        Polinom p2=new Polinom();
        p1=c.getP1();
        p2=c.getP2();
        Polinom p3=new Polinom();
        p3=c.plus(p1,p2);

        String s=c.afisare(p3);
        assertNotNull(s);
        assertEquals(s,"+3.0X^4 +4.0X^2 -1.0X^1 +6.0X^0 ");
        nrTesteCuSucces++;
    }

    @org.junit.jupiter.api.Test
    void ori() {
        c.citire("2*X^4 +1*X^2 -1*X^1 5*X^0",1);
        c.citire("3*X^2  1*X^1 1*X^0",2);
        Polinom p1=new Polinom();
        Polinom p2=new Polinom();
        p1=c.getP1();
        p2=c.getP2();
        Polinom p3=new Polinom();
        p3=c.ori(p1,p2);

        String s=c.afisare(p3);
        assertNotNull(s);
        assertEquals(s,"+6.0X^6 +2.0X^5 +5.0X^4 -2.0X^3 +15.0X^2 +4.0X^1 +5.0X^0 ");
        nrTesteCuSucces++;
    }

    @org.junit.jupiter.api.Test
    void derivare() {
        c.citire("4*X^17 +6*X^15 -3*X^7 +6*X^5 +2*X^0",1);
        Polinom p1=new Polinom();
        p1=c.getP1();

        Polinom p3=new Polinom();
        p3=c.derivare(p1);


        String s=c.afisare(p3);
        assertNotNull(s);
        assertEquals(s,"+68.0X^16 +90.0X^14 -21.0X^6 +30.0X^4 ");
        nrTesteCuSucces++;
    }

    @org.junit.jupiter.api.Test
    void integrare() {
        c.citire("4*X^17 +6*X^15 -3*X^7 +6*X^5 +2*X^0",1);
        Polinom p1=new Polinom();
        p1=c.getP1();

        Polinom p3=new Polinom();
        p3=c.integrare(p1);


        String s=c.afisare(p3);
        assertNotNull(s);
        assertEquals(s,"+0.22222222X^18 +0.375X^16 -0.375X^8 +1.0X^6 +2.0X^1 ");
        nrTesteCuSucces++;
    }

    @org.junit.jupiter.api.Test
    void div() {
        c.citire("1*X^4 -1*X^3 2*X^2 -1*X^1 3*X^0",1);
        c.citire("1*X^1 -1*X^0",2);
        Polinom p1=new Polinom();
        Polinom p2=new Polinom();
        p1=c.getP1();
        p2=c.getP2();
        Polinom p3=new Polinom();
        p3=c.div(p1,p2);

        String cat=c.afisare(p3);
        String rest=c.afisare(c.getP4());
        assertEquals(cat,"+1.0X^3 +2.0X^1 +1.0X^0 ");
        assertEquals(rest,"+4.0X^0 ");
        nrTesteCuSucces++;

    }
}