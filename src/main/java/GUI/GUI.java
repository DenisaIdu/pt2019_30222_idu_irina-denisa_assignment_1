package GUI;

import Control.Control;
import Model.Monom;
import Model.Polinom;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

public class GUI {
    public void ShowGUI(){
        final JFrame f=new JFrame("Calculator de polinoamne");
        f.setLayout(null);
        f.setSize(700,600);



        final JTextField tf1,tf2,tfrez,tfg1,tfg2,tfrest;
        final JLabel l1,l2,lrez,lrest;
        final JButton bplus,bminus,bori,bdiv,bder,bint,g1,g2,bc;

        l1=new JLabel("Polinom 1:");
        l2=new JLabel("Polinom 2:");
        lrez=new JLabel("Rezultat:");
        lrest=new JLabel("Rest");
        tfrest=new JTextField();
        tf1=new JTextField();
        tf2=new JTextField();
        tfg1=new JTextField();
        tfg2=new JTextField();
        tfrez=new JTextField();
        g1=new JButton("Genereaza");
        g2=new JButton("Genereaza");
        bplus=new JButton("+");
        bminus=new JButton("-");
        bori=new JButton("X");
        bdiv=new JButton("/");
        bder=new JButton("Derivare");
        bint=new JButton("Integrare");
        bc=new JButton("Clear");

        l1.setBounds(50,80,100,50);
        l2.setBounds(50,130,100,50);
        tf1.setBounds(150,90,150,30);
        tf2.setBounds(150,140,150,30);
        bplus.setBounds(190,210,50,50);
        bminus.setBounds(190,280,50,50);
        bori.setBounds(250,210,50,50);
        bdiv.setBounds(250,280,50,50);
        bder.setBounds(310,210,100,50);
        bint.setBounds(310,280,100,50);
        lrez.setBounds(50,380,100,50);
        lrest.setBounds(50,410,100,50);
        tfrez.setBounds(150,390,300,30);
        tfrest.setBounds(150,430,300,30);
        g1.setBounds(310,90,100,30);
        g2.setBounds(310,140,100,30);
        tfg1.setBounds(420,90,200,30);
        tfg2.setBounds(420,140,200,30);
        bc.setBounds(500,250,100,50);




        f.add(l1);
        f.add(l2);
        f.add(tf1);
        f.add(tf2);
        f.add(bplus);
        f.add(bminus);
        f.add(bori);
        f.add(bdiv);
        f.add(bint);
        f.add(bder);
        f.add(lrez);
        f.add(tfrez);
        f.add(g1);
        f.add(g2);
        f.add(tfg1);
        f.add(tfg2);
        f.add(bc);
        f.add(lrest);
        f.add(tfrest);

        final Control c=new Control();

        g1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    String s=new String();
                    s=tf1.getText();
                    c.citire(s,1);
                    tfg1.setText(c.afisare(c.getP1()));
                }catch (Exception a){
                    Frame f = new JFrame();
                    JOptionPane.showMessageDialog(f, "Polinoamele nu s-au putut citi!!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });


        g2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String s=new String();
                    s=tf2.getText();
                    c.citire(s,2);
                    tfg2.setText(c.afisare(c.getP2()));
                }catch (Exception a){
                    Frame f = new JFrame();
                    JOptionPane.showMessageDialog(f, "Introduceti doar numere!!", "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        });


        bplus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Polinom p1=new Polinom();
                    Polinom p2=new Polinom();
                    p1=c.getP1();
                    p2=c.getP2();

                    Polinom p3=new Polinom();
                    p3=c.plus(p1,p2);
                    tfrez.setText(c.afisare(p3));
                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });

        bc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tf1.setText("");
                tf2.setText("");
                tfg1.setText("");
                tfg2.setText("");
                tfrez.setText("");
                c.setP1(null);
                c.setP2(null);
                c.setP3(null);
                c.setP4(null);
                tfrest.setText("");

            }
        });

        bminus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Polinom p1=new Polinom();
                    Polinom p2=new Polinom();
                    p1=c.getP1();
                    p2=c.getP2();
                    Polinom p3=c.minus(p1,p2);
                    tfrez.setText(c.afisare(p3));
                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });


        bori.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Polinom p1=new Polinom();
                    Polinom p2=new Polinom();
                    p1=c.getP1();
                    p2=c.getP2();
                    Polinom p3=c.ori(p1,p2);
                    tfrez.setText(c.afisare(p3));

                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });


        bder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Polinom p1=new Polinom();
                    p1=c.getP1();

                    Polinom p2=c.derivare(p1);
                    tfrez.setText(c.afisare(p2));
                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });

        bint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Polinom p1=new Polinom();
                    p1=c.getP1();
                    Polinom p2=c.integrare(p1);
                    tfrez.setText(c.afisare(p2));
                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);
                }


            }
        });


        bdiv.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Polinom p1=new Polinom();
                    Polinom p2=new Polinom();
                    p1=c.getP1();
                    p2=c.getP2();
                    Polinom p3=new Polinom();
                    p3=c.div(p1,p2);
                    tfrez.setText(c.afisare(p3));
                    tfrest.setText(c.afisare(c.getP4()));
                }catch (Exception a){
                    JOptionPane.showMessageDialog(f, "Prima data trbuie sa generati polinoamele!!", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        });

        f.setVisible(true);

    }



}
