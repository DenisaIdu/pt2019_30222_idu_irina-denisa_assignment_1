package Model;

import java.util.ArrayList;

public class Polinom {
    private ArrayList<Monom> polinom=new ArrayList<Monom>();

    public void adaugaElement(Monom m){
        polinom.add(m);
    }

    public ArrayList<Monom> getPolinom() {
        return polinom;
    }

}
