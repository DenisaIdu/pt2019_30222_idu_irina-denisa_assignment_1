package Model;

public class Monom {
    private int grad;
    private float coeficient;


    public Monom(int grad,float coeficient){
        this.grad=grad;
        this.coeficient=coeficient;
    }

    public int getGrad() {
        return grad;
    }

    public float getCoeficient() {
        return coeficient;
    }


    public void setCoeficient(float coeficient) {
        this.coeficient = coeficient;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

}
