package Control;

import Model.Monom;
import Model.Polinom;

import java.io.Serializable;
import java.util.StringTokenizer;

public class Control implements Serializable {
    Polinom p1=new Polinom();
    Polinom p2=new Polinom();
    Polinom p3=new Polinom();
    Polinom p4=new Polinom();


    public String afisare(Polinom p){
        String s=new String();
        for (int i=0;i<p.getPolinom().size();i++)
        {
            if(p.getPolinom().get(i).getCoeficient()>0 ){
                s=s+"+"+p.getPolinom().get(i).getCoeficient()+"X^"+p.getPolinom().get(i).getGrad()+" ";
            }
            else{
                if(p.getPolinom().get(i).getCoeficient()<0){
                    s=s+p.getPolinom().get(i).getCoeficient()+"X^"+p.getPolinom().get(i).getGrad()+" ";
                }
            }
        }
        return s;
    }

    public void citire(String s,int nrpol) {
        Polinom p = new Polinom();

        StringTokenizer tok = new StringTokenizer(s, " ");

        while (tok.hasMoreElements()) {
            String st = tok.nextElement().toString();
            String coef = new String();
            String putere = new String();
            int sem = 0;
            int ok = 0;
            for (int i = 0; i < st.length(); i++) {

                if (st.charAt(i) == '^') {
                    ok = 1;
                } else {

                    if (st.charAt(i) >= '0' && st.charAt(i) <= '9') {
                        if (ok == 0) {
                            coef = coef + st.charAt(i);
                        } else {
                            putere = putere + st.charAt(i);
                        }
                    }
                }
                if (st.charAt(i) == '-') {
                    sem = 1;
                }
            }

            float coef1 = Integer.parseInt(coef);
            int putere1 = Integer.parseInt(putere);
            if (sem == 1)
                coef1 = -coef1;
            Monom m = new Monom(putere1, coef1);
            p.adaugaElement(m);


            if (nrpol == 1) {
                this.p1 = p;
            } else if (nrpol == 2) {
                this.p2 = p;
            }
        }
    }

    public Polinom minus(Polinom p1,Polinom p2){

        for(int i=0;i<p2.getPolinom().size();i++){
            p2.getPolinom().get(i).setCoeficient(-p2.getPolinom().get(i).getCoeficient());
        }
        Polinom rez=new Polinom();
        rez=plus(p1,p2);
        return rez;
    }
    public Polinom plus(Polinom p1,Polinom p2){
        int m=p1.getPolinom().size(); //dimensiunea primului polinom
        int n=p2.getPolinom().size(); //dimensiunea celui de al doilea polinom
        int i=0,j=0;
        Polinom p=new Polinom();

        while (i<m && j<n)
        {
            if(p1.getPolinom().get(i).getGrad()>p2.getPolinom().get(j).getGrad()){
                p.adaugaElement(p1.getPolinom().get(i));
                i++;
            }
            else
            if(p1.getPolinom().get(i).getGrad()<p2.getPolinom().get(j).getGrad()){
                p.adaugaElement(p2.getPolinom().get(j));
                j++;
            }
            else{
                int grad=p1.getPolinom().get(i).getGrad();
                float coef=p1.getPolinom().get(i).getCoeficient()+p2.getPolinom().get(j).getCoeficient();
                Monom mon=new Monom(grad,coef);
                p.adaugaElement(mon);
                i++;
                j++;
            }
        }
        if(i<m){
            while (i<m){
                p.adaugaElement(p1.getPolinom().get(i));
                i++;
            }
        }
        if(j<n){
            while (j<n){
                p.adaugaElement(p2.getPolinom().get(j));
                j++;
            }
        }
        return p;
    }

    public Polinom ori(Polinom p1,Polinom p2){
        Polinom p=new Polinom();

        for(int i=0;i<p1.getPolinom().size();i++){
            for(int j=0;j<p2.getPolinom().size();j++){
                int grad=p1.getPolinom().get(i).getGrad()+p2.getPolinom().get(j).getGrad();
                float coef=p1.getPolinom().get(i).getCoeficient()*p2.getPolinom().get(j).getCoeficient();
                Monom m=new Monom(grad,coef);
                p.adaugaElement(m);
            }
        }

        for(int i=0;i<p.getPolinom().size()-1;i++)
            for(int j=i+1;j<p.getPolinom().size();j++)
                if(p.getPolinom().get(i).getGrad()==p.getPolinom().get(j).getGrad()){
                    p.getPolinom().get(i).setCoeficient(p.getPolinom().get(i).getCoeficient()+p.getPolinom().get(j).getCoeficient());
                    p.getPolinom().remove(j);
                }




        return p;
    }

    public Polinom derivare(Polinom p1){
        Polinom p= new Polinom();
        for(int i=0;i<p1.getPolinom().size();i++){
            int grad=p1.getPolinom().get(i).getGrad()-1;
            float coef=p1.getPolinom().get(i).getCoeficient()*p1.getPolinom().get(i).getGrad();
            Monom m=new Monom(grad,coef);
            p.adaugaElement(m);
        }
        return p;
    }

    public Polinom integrare(Polinom p1){
        Polinom p=new Polinom();
        for(int i=0;i<p1.getPolinom().size();i++){
            int grad=p1.getPolinom().get(i).getGrad()+1;
            float coef=p1.getPolinom().get(i).getCoeficient()/(p1.getPolinom().get(i).getGrad()+1);
            Monom m=new Monom(grad,coef);
            p.adaugaElement(m);
        }
        return p;
    }

    public Polinom div(Polinom p1, Polinom p2){
        Polinom cat=new Polinom();
        Polinom rest=new Polinom();


        if(p2.getPolinom().get(0).getGrad()==0){
            cat=p1;
            Monom monom=new Monom(1,0);
            rest.adaugaElement(monom);
        }
        else {
            while (p1.getPolinom().get(0).getGrad() >= p2.getPolinom().get(0).getGrad()) {
                float coef = p1.getPolinom().get(0).getCoeficient() / p2.getPolinom().get(0).getCoeficient();
                int grad = p1.getPolinom().get(0).getGrad() - p2.getPolinom().get(0).getGrad();
                Monom m = new Monom(grad, coef);
                cat.adaugaElement(m);
                Polinom pori = new Polinom();
                pori.adaugaElement(m);

                Polinom p = new Polinom();
                p = ori(pori, p2);
                rest = minus(p1, p);
                for (int i = 0; i < rest.getPolinom().size(); i++) {
                    if (rest.getPolinom().get(i).getCoeficient() == 0) {
                        rest.getPolinom().remove(i);
                    }
                }
                p1 = rest;
            }
        }
        setP3(cat);
        setP4(rest);

        return cat;

    }

    public Polinom getP1() {
        return p1;
    }

    public Polinom getP2() {
        return p2;
    }

    public void setP1(Polinom p1) {
        this.p1 = p1;
    }

    public void setP2(Polinom p2) {
        this.p2 = p2;
    }

    public Polinom getP3() {
        return p3;
    }

    public Polinom getP4() {
        return p4;
    }

    public void setP3(Polinom p3) {
        this.p3 = p3;
    }

    public void setP4(Polinom p4) {
        this.p4 = p4;
    }
}